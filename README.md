## A simple GitLab Serverless function using the OpenFaaS ruby runtime

See the [documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) for more information.

The runtime files can be found here: https://github.com/openfaas/templates/tree/master/template/ruby

### Usage

This function will respond with `Hello, world`

Run using curl (replace URL with your Knative service endpoint URL)

`curl http://functions-hello.functions-10069011.example.com`